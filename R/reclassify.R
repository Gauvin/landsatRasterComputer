

binaryReclassification <- function(rast,threshLower=0,threshUpper=Inf){

  require(raster)

  if(is.finite(threshUpper)){
    dfClass <- cbind(from = c(-Inf,threshLower,threshUpper),
                     to = c(threshLower,threshUpper,Inf ),
                     become = c(0,1,0))
  }else{
    dfClass <- cbind(from = c(-Inf,threshLower),
                     to = c(threshLower,Inf ),
                     become = c(0,1))
  }



  rast <- reclassify(rast,dfClass)
  vals <- unique(values(rast))
  stopifnot( all(vals[!is.na(vals)] %in% c(0,1)) )

  return(rast)

}
